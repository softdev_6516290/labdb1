/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Acer
 */
public class InsertDatabaes {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:Dcoffee.db";
        //ConnectDatabaes
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            System.out.println("Connection to SQLite has been estabish.");
            return;
        }

        //Insert
        String sql = "INSERT INTO CATEGORY(CATEGORY_ID, CATEGORY_NAME) VALUES(?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            stmt.setString(2, "Candy");
            int status = stmt.executeUpdate();
            //ResultSet key = stmt.getGeneratedKeys();
            //key.next();
            //System.out.println(""+key.getInt(1));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //CloseDatabaes
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}